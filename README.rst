fs.cloud
========

|grade| |coverage|

.. |grade| image:: https://img.shields.io/codacy/grade/e27821fb6289410b8f58338c7e0bc686/master.svg
   :target: https://www.codacy.com/app/althonos/fs-cloud/dashboard

.. |coverage| image:: https://img.shields.io/codacy/coverage/b7dda44f49784fdbbb20e6103b7753d4/master.svg
   :target: https://www.codacy.com/app/althonos/fs-cloud/dashboard
