# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import sys
import unittest

#from nose.plugins.attrib import attr
from fs import errors
from fs.cloud import dropboxfs
from fs.test import FSTestCases

TEST_TOKEN = os.environ.get('DROPBOX_TEST_TOKEN')
TEST_DIR = "/{ci}-{version}".format(
    version=sys.version.split(' ')[0],
    ci="travis" if os.environ.get('TRAVIS') is not None
    else "gitlab" if os.environ.get('GITLAB_CI') is not None
    else "user")

#@attr('slow')
@unittest.skipIf(TEST_TOKEN is None, 'cannot find Dropbox OAuth token')
class TestDropboxFS(FSTestCases, unittest.TestCase):

    @staticmethod
    def make_fs():
        return dropboxfs.DropboxFS(TEST_TOKEN).makedir(TEST_DIR, recreate=True)

    @staticmethod
    def destroy_fs(fs):
        fs.close()

    @staticmethod
    def clean_fs(fs):
        for entry in fs.delegate_fs()._dropbox.files_list_folder(TEST_DIR).entries:
            fs.delegate_fs()._dropbox.files_delete(entry.path_display)

    @classmethod
    def setUpClass(cls):
        cls.fs = cls.make_fs()
        cls._dropbox = cls.fs.delegate_fs()._dropbox

    @classmethod
    def tearDownClass(cls):
        cls._dropbox.files_delete(TEST_DIR)
        cls.destroy_fs(cls.fs)
        del cls.fs

    def setUp(self):
        if self.fs.isclosed():
            self.setUpClass()

    def tearDown(self):
        self.clean_fs(self.fs)

    def test_geturl_sharing(self):

        #print(self._dropbox.sharing_get_shared_links().links)

        # Create a test file
        self.fs.setbytes('/foo', b'barbaz')

        # Check that the file has not share url before sharing it
        self.assertFalse(self.fs.hasurl('/foo', purpose='share'))

        # Try revoking a link that does not exist
        with self.assertRaises(errors.NoURL):
            self.fs.geturl('/foo', purpose='revoke')

        # Create a share url and try reading the file from it
        url = self.fs.geturl('/foo', purpose='share')

        # Check that the file now has a share url
        self.assertTrue(self.fs.hasurl('/foo', purpose='share'))

        # Check that Dropbox is aware of the new link
        self.assertIn(url, [pathlink.url
            for pathlink in self._dropbox.sharing_get_shared_links().links])

        # Revoke the link and check it cannot be used anymore
        self.fs.geturl('/foo', purpose='revoke')
        self.assertFalse(self.fs.hasurl('/foo', purpose='share'))
        self.assertNotIn(url, [pathlink.url
            for pathlink in self._dropbox.sharing_get_shared_links().links])
