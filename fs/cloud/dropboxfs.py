# coding: utf-8
from __future__ import unicode_literals
from __future__ import absolute_import

import io
import six
import dropbox
import datetime
import functools
import contextlib
import collections

from . import __version__
from .. import errors
from ..mode import Mode
from ..enums import ResourceType
from ..base import FS
from ..info import Info
from ..time import datetime_to_epoch
from ..path import dirname


class _DropboxErrorUnwrapper(collections.Callable):

    @classmethod
    def _unwrap(cls, error, path=None, api_error=None):
        for k, callback in six.iteritems(cls.error_map()):
            if getattr(error, k, lambda: False)():
                return callback(error, path, api_error)
        return errors.OperationFailed(path, api_error)

    @classmethod
    def error_map(cls):
        return {
            # 'is_not_closed': TODO,
            # 'is_incorrect_offset': TODO,
            'is_file': lambda e, p, a:
                errors.FileExists(p, a),
            'is_to': lambda e, p, a:
                cls._unwrap(e.get_to(), p, a),
            'is_not_file': lambda e, p, a:
                errors.FileExpected(p, a),
            'is_folder': lambda e, p, a:
                errors.DirectoryExists(p, a),
            'is_path': lambda e, p, a:
                cls._unwrap(e.get_path(), p, a),
            'is_malformed_path': lambda e, p, a:
                errors.InvalidPath(p, a),
            'is_not_found': lambda e, p, a:
                errors.ResourceNotFound(p, a),
            'is_disallowed_name': lambda e, p, a:
                errors.InvalidPath(p, a),
            'is_not_folder': lambda e, p, a:
                errors.DirectoryExpected(p, a),
            'is_invalid_path_root': lambda e, p, a:
                errors.InvalidPath(p, a),
            'is_file_ancestor': lambda e, p, a:
                errors.DirectoryExpected(p, a),
            'is_conflict': lambda e, p, a:
                cls._unwrap(e.get_conflict(), p, a),
            'is_restricted_content': lambda e, p, a:
                errors.PermissionDenied(p, a),
            'is_no_write_permission': lambda e, p, a:
                errors.PermissionDenied(p, a),
            'is_from_write': lambda e, p, a:
                cls._unwrap(e.get_from_write(), p, a),
            'is_path_write': lambda e, p, a:
                cls._unwrap(e.get_path_write(), p, a),
            'is_insufficient_space': lambda e, p, a:
                errors.InsufficientStorage(p, a),
            'is_from_lookup': lambda e, p, a:
                cls._unwrap(e.get_from_lookup(), p, a),
            'is_path_lookup': lambda e, p, a:
                cls._unwrap(e.get_path_lookup(), p, a),
            'is_lookup_failed': lambda e, p, a:
                cls._unwrap(e.get_lookup_failed(), p, a),
            'is_too_many_files': lambda e, p, a:
                errors.Unsupported(p, a, "too many files"),
            'is_closed': lambda e, p, a:
                errors.ResourceLocked(p, a, "upload session is closed"),
            'is_cant_copy_shared_folder': lambda e, p, a:
                errors.Unsupported(p, a, "cannot copy shared folder '{path}'"),
            'is_cant_move_folder_into_itself': lambda e, p, a:
                errors.Unsupported(p, a, "cannot move '{path}' into itself"),
            'is_cant_nest_shared_folder': lambda e, p, a:
                errors.Unsupported(p, a, "cannot nest shared folder '{path}'"),
        }

    def __call__(_self, meth):
        @functools.wraps(meth)
        def new_method(self, path, *args, **kwargs):
            try:
                err = None
                result = meth(self, path, *args, **kwargs)
            except dropbox.exceptions.ApiError as api_err:
                err = _self._unwrap(api_err.error, path, api_err)
                six.raise_from(err, None)
            else:
                return result
        return new_method

    @classmethod
    def decorate(cls, other):
        self = cls()
        for name, method in six.iteritems(vars(other)):
            if not name.startswith('_') and callable(method):
                setattr(other, name, self(method))
        return other

    @classmethod
    @contextlib.contextmanager
    def transmute(cls, source_errors, dest_error, mute=False):
        try:
            yield
        except dropbox.exceptions.ApiError as api_err:
            err = cls._unwrap(api_err.error, None, api_err)
            if isinstance(err, source_errors):
                if not mute:
                    raise dest_error or api_err
            else:
                raise


class _DropboxFile(io.IOBase):

    __slots__ = ('path', 'mode', '_closed', '_mtime', '_atime',
                 '_bytes_io', '_dropboxfs', '_dropbox')

    @_DropboxErrorUnwrapper()
    def __init__(self, path, fs, mode=u'rb', buffering=-1, **options):
        super(_DropboxFile, self).__init__()
        self.path = _path = fs.validatepath(path)
        self.mode = _mode = Mode(mode)
        self._mtime = None
        self._dropboxfs = fs
        self._dropbox = fs._dropbox

        if 't' in mode:
            raise ValueError

        # Touch the file to see if it exists
        try:
            metadata = self._dropbox.files_get_metadata(_path)
        except dropbox.exceptions.ApiError as api_err:
            metadata = None
            if not api_err.error.is_path() or not api_err.error.get_path().is_not_found():
                raise
        else:
            self._mtime = getattr(metadata, 'client_modified', None)

        # Raise and error if path goes to a directory
        # TODO: also check path components ?
        if isinstance(metadata, dropbox.files.FolderMetadata):
            raise errors.FileExpected(path)

        # Raise error for 'r' if the file does not exist
        if metadata is None:
             if mode in 'rb':
                 raise errors.ResourceNotFound(path)

        # Raise error for 'x' if the file does exist
        if metadata is not None and _mode.exclusive:
            raise errors.FileExists(path)

        # Get the initial content if needed
        if metadata is not None and (_mode.appending or _mode.reading):
            initial_content = self._dropbox.files_download(path)[1].content
        else:
            initial_content = b''

        # Create the content
        self._bytes_io = six.BytesIO()
        self._bytes_io.write(initial_content)

        # Place the cursor at the beginning if rb or rb+
        if _mode.reading:
            self._bytes_io.seek(0)

    def __len__(self):
        start_pos = self._bytes_io.tell()
        self._bytes_io.seek(0, 2)
        length = self._bytes_io.tell()
        self._bytes_io.seek(start_pos)
        return length

    def _on_modify_file(self):
        self._on_access_file()
        self._mtime = datetime.datetime.now()

    def _on_access_file(self):
        self._atime = datetime.datetime.now()

    def flush(self):
        self._bytes_io.flush()

    def readable(self):
        return self.mode.reading

    def writable(self):
        return self.mode.writing

    def seekable(self):
        return True

    def __iter__(self):
        self._on_access_file()
        return self._bytes_io.__iter__()

    def read(self, size=None):
        self._on_access_file()
        return self._bytes_io.read(size)

    if six.PY3:
        def read1(self, size):
            self._on_access_file()
            return self._bytes_io.read1(size)

    def readinto(self, buffer):
        self._on_access_file()
        return self._bytes_io.readinto(buffer)

    def readline(self, size=None):
        self._on_access_file()
        return self._bytes_io.readline(size)

    def readlines(self, size=None):
        self._on_access_file()
        return self._bytes_io.readlines(size)

    def seek(self, pos, whence=0):
        if whence not in {0, 1, 2}:
            raise ValueError("Invalid whence ({}, should be 0, 1 \
                             or 2)".format(whence))
        return self._bytes_io.seek(pos, whence)

    def truncate(self, size=None):
        self._on_modify_file()
        if size is not None and size > len(self):
            self._bytes_io.write(b'\0'*(size-len(self)))
            return size
        else:
            return self._bytes_io.truncate(size)

    def tell(self):
        return self._bytes_io.tell()

    def write(self, b):
        self._on_modify_file()
        return self._bytes_io.write(b)

    def writelines(self, lines):
        self._on_modify_file()
        return self._bytes_io.writelines(lines)

    def close(self):
        if not self.closed:
            length = len(self)
            if length > 1.5E8:  # 150MB
                self._upload_with_session(self.path, length)
            else:
                self._upload(self.path, length)
            super(_DropboxFile, self).close()
            self._bytes_io.close()

    @_DropboxErrorUnwrapper()
    def _upload(self, path, length):
        b_io = self._bytes_io
        b_io.seek(0)
        self._dropbox.files_upload(
            b_io.read(), path, autorename=False,
            mode=dropbox.files.WriteMode.overwrite, mute=False,
            client_modified=self._mtime or datetime.datetime.now())

    @_DropboxErrorUnwrapper()
    def _upload_with_session(self, path, length):
        CHUNK_SIZE = 1.5E8
        b_io = self._bytes_io
        b_io.seek(0)

        upload_session = self._dropbox.files_upload_session_start(b'')
        cursor = dropbox.files.UploadSessionCursor(
            upload_session.session_id, b_io.tell()
        )
        commit = dropbox.files.CommitInfoWithProperties(
            path=path, mode=dropbox.files.WriteMode.overwrite, autorename=None,
            client_modified=self._mtime or datetime.datetime.now(),
            mute=None, property_groups=None
        )
        while True:
            if length - b_io.tell() <= CHUNK_SIZE:
                self._dropbox.files_upload_session_finish(
                    b_io.read(CHUNK_SIZE), cursor, commit
                )
                break
            else:
                self._dropbox.files_upload_session_append_v2(
                    b_io.read(CHUNK_SIZE), cursor
                )
                cursor.offset = b_io.tell()


@_DropboxErrorUnwrapper.decorate
class DropboxFS(FS):

    _meta = {
        'case_insensitive': True,
        'network': True,
        'read_only': False,
        'supports_rename': False,
        'thread_safe': True,
        'unicode_paths': True,
        'virtual': False,
        'invalid_path_chars':
            ':*?"<>|' + ''.join(chr(x) for x in range(32)),
    }

    _user_agent = "fs.cloud.dropboxfs/{}".format(__version__)

    def __init__(self, oauth_token=None, _user_agent=None, session=None, headers=None):
        super(DropboxFS, self).__init__()
        self._dropbox = dropbox.Dropbox(
            oauth_token,
            _user_agent or self._user_agent,
            session,
            headers)
        self._username = self._dropbox.users_get_current_account().name.familiar_name

    def __repr__(self):
        return "DropboxFS(user={!r})".format(self._username)

    def __str__(self):
        return "<dropboxfs '{}'>".format(self._username)

    @contextlib.contextmanager
    def _sanitize(self, *paths):
        self.check()
        paths = [self.validatepath(path) for path in paths]
        with self._lock:
            yield paths[-1] if len(paths) == 1 else paths

    def _relocate(self, src_path, dst_path, overwrite, relocation_method):
        with self._sanitize(src_path, dst_path) as (src_path, dst_path):
            if self.exists(dst_path):
                if not overwrite:
                    raise errors.DestinationExists(dst_path)
                else:
                    self.remove(dst_path)
            dst_parent = dirname(dst_path)
            if dst_parent not in '/' and not self.exists(dst_parent):
                raise errors.ResourceNotFound(dst_parent)

            with _DropboxErrorUnwrapper.transmute(
                    (errors.FileExists, errors.DirectoryExists),
                    errors.DestinationExists(dst_path)):
                relocation_method(src_path, dst_path)

    def copy(self, src_path, dst_path, overwrite=False):
        self._relocate(src_path, dst_path, overwrite, self._dropbox.files_copy)

    def getinfo(self, path, namespaces=None):
        with self._sanitize(path) as path:
            if path in '/':
                raw_info = {
                    "basic":
                    {
                        "name": "",
                        "is_dir": True,
                    },
                    "details":
                    {
                        "type": int(ResourceType.directory)
                    },
                }

            else:
                metadata = self._dropbox.files_get_metadata(path)
                if isinstance(metadata, dropbox.files.FolderMetadata):
                    raw_info = {
                        'basic': {'name': metadata.name, 'is_dir': True},
                        "details": {"type": int(ResourceType.directory)},
                    }
                else:
                    raw_info = {
                        'basic': {'name': metadata.name, 'is_dir': False},
                        'details': {
                            'type': int(ResourceType.file),
                            'size': metadata.size,
                            'mtime':
                                datetime_to_epoch(metadata.client_modified),
                        }
                    }

                raw_info['dropbox'] = {
                    attr: getattr(metadata, attr)
                    for attr in dir(metadata)
                    if not attr.startswith('_')
                }

                for k in ('client_modified', 'server_modified'):
                    if k in raw_info['dropbox']:
                        raw_info['dropbox'][k] = \
                            datetime_to_epoch(raw_info['dropbox'][k])

        return Info(raw_info)

    def listdir(self, path):
        with self._sanitize(path) as path:
            if path in '/':
                list_result = self._dropbox.files_list_folder('')
            else:
                list_result = self._dropbox.files_list_folder(path)
            entries = list_result.entries
            while list_result.has_more:
                list_result = self._dropbox.files_list_folder_continue(list_result.cursor)
                entries.extend(list_result.entries)
            return [e.name for e in entries]

    def makedir(self, path, permissions=None, recreate=False):
        with self._sanitize(path) as path:
            if path not in '/':
                parent = dirname(path)
                if parent not in '/':
                    self._dropbox.files_get_metadata(parent)

                with _DropboxErrorUnwrapper.transmute(
                        errors.FileExists, errors.DirectoryExists(path)):
                    with _DropboxErrorUnwrapper.transmute(
                            errors.DirectoryExists, None, mute=recreate):
                        self._dropbox.files_create_folder(path)

            else:
                if not recreate:
                    raise errors.DirectoryExists(path)

            return self.opendir(path)

    def makedirs(self, path, permissions=None, recreate=False):
        with self._sanitize(path) as path:
            if path not in '/':
                if not recreate:
                    parent = dirname(path)
                    with _DropboxErrorUnwrapper.transmute(
                            errors.ResourceNotFound, None, mute=True):
                        if parent not in '/' and self.isfile(parent):
                                raise errors.DirectoryExpected(path)
                with _DropboxErrorUnwrapper.transmute(
                        errors.DirectoryExists, None, mute=recreate):
                    self._dropbox.files_create_folder(path)
            return self.opendir(path)

    def move(self, src_path, dst_path, overwrite=False):
        self._relocate(src_path, dst_path, overwrite, self._dropbox.files_move)

    def openbin(self, path, mode='r', buffering=-1, **options):
        with self._sanitize(path) as path:
            if self.isdir(path):
                raise errors.FileExpected(path)
            return _DropboxFile(path, self, mode, buffering, **options)

    def remove(self, path):
        with self._sanitize(path) as path:
            if path in '/':
                raise errors.FileExpected(path)
            metadata = self._dropbox.files_get_metadata(path)
            if isinstance(metadata, dropbox.files.FolderMetadata):
                raise errors.ResourceInvalid(path)
            self._dropbox.files_delete(path)

    def removedir(self, path, force=False):
        with self._sanitize(path) as path:
            if path in '/':
                raise errors.RemoveRootError(path)
            if not force and self._dropbox.files_list_folder(path).entries:
                raise errors.DirectoryNotEmpty(path)
            self._dropbox.files_delete(path)

    def setinfo(self, path, info):
        with self._sanitize(path) as path:
            if not self.exists(path):
                raise errors.ResourceNotFound(path)

    def hasurl(self, path, purpose='download'):
        if purpose=='share':
            with self._sanitize(path):
                shared = self._dropbox.sharing_list_shared_links(path)
                return bool(shared.links)
        else:
            return super(DropboxFS, self).hasurl(path, purpose)

    def geturl(self, path, purpose='download'):
        """
        Supported purposes:
        * download: return a link to the resource. Link is
          temporary and deleted after a few hours.
        * sharing: return a link for sharing a resource
        * revoke: revoke links created with sharing
        """
        with self._sanitize(path) as path:
            if purpose == 'download':
                return self._dropbox.files_get_temporary_link(path).link
            elif purpose == 'share':
                return self._get_share_url(path)
            elif purpose == 'revoke':
                self._revoke_share_url(path)
            else:
                raise errors.NoURL(path, purpose)

    def _get_share_url(self, path):
        #short_url = options.get('short_url', True)
        shared = self._dropbox.sharing_list_shared_links(path)
        return shared.links[0].url if shared.links \
               else self._dropbox.sharing_create_shared_link(
                    path, short_url=False).url

    def _revoke_share_url(self, path):
        shared = self._dropbox.sharing_list_shared_links(path)
        links = shared.links
        while shared.has_more:
            shared = self._dropbox.sharing_list_shared_links(path, shared.cursor)
            links.extend(shared.links)
        if not links:
            raise errors.NoURL(path, 'share')
        for link in links:
            self._dropbox.sharing_revoke_shared_link(link.url)
